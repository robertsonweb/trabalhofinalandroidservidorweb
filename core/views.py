# coding: utf-8
from datetime import datetime, timedelta
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import check_password
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from models import *

#HOME
@login_required
def home(request):
    return render(request,'base.html')

#MEU LOCAL
@login_required
def meu_local(request):
    return render(request,'meu_local.html')

#SOLICITACOES PENDENTES DO TAXI
@login_required
def taxi_solicitacoes_pendentes(request):
    ls = Solicitacao.objects.filter(taxi=Taxi.objects.get(usuario=request.user), data_conclusao__isnull=True)
    return render(request, 'taxi_solicitacoes_pendentes.html', {'ls':ls})

#SOLICITACOES ATENDIDAS DO TAXI
@login_required
def taxi_solicitacoes_atendidas(request):
    ls = Solicitacao.objects.filter(taxi=Taxi.objects.get(usuario=request.user), data_conclusao__isnull=False)
    return render(request, 'taxi_solicitacoes_atendidas.html', {'ls':ls})

@login_required
def taxi_solicitacao(request,solicitacao_id):
    return render(request, 'taxi_solicitacao.html', {'solicitacao':Solicitacao.objects.get(id=solicitacao_id)})

@login_required
def taxi_solicitacao_alterar_situacao(request,solicitacao_id):
    solicitacao = Solicitacao.objects.get(pk=solicitacao_id)
    if solicitacao.data_conclusao:
        solicitacao.data_conclusao = None
    else:
        solicitacao.data_conclusao = datetime.now()
    solicitacao.save()
    return taxi_solicitacoes_pendentes(request)

def taxi_alterar_minha_situacao(request):
    taxi = Taxi.objects.get(usuario=request.user)
    if taxi.situacao == 'L':
        taxi.situacao = 'O'
    else:
        taxi.situacao = 'L'
    taxi.save()
    return home(request)


#TAXI
def taxi_buscar(request, taxi_id):
    try:
        return HttpResponse(serializers.serialize("json", Taxi.objects.filter(pk=taxi_id)), content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")


def buscar_taxi_pela_situacao(situacao):
    return Taxi.objects.filter(situacao=situacao)

def taxi_livres(request):
    return HttpResponse(serializers.serialize("json", buscar_taxi_pela_situacao('L')), content_type="application/json")


def taxi_ocupados(request):
    return HttpResponse(serializers.serialize("json", buscar_taxi_pela_situacao('O')), content_type="application/json")

def taxi_alterar_situacao(request,taxi_id):
    try:
        taxi = Taxi.objects.get(pk=taxi_id)
        if taxi.situacao == 'L':
            taxi.situacao = 'O'
        else:
            taxi.situacao = 'L'

        taxi.save()

        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except:
        return HttpResponse('{"resultado":"ERRO"}', content_type="application/json")


def taxi_alterar_posicao(request,taxi_id, latitude, longitude):
    try:
        taxi            = Taxi.objects.get(pk=taxi_id)
        taxi.latitude   = latitude
        taxi.longitude  = longitude
        taxi.save()

        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except:
        return HttpResponse('{"resultado":"ERRO"}', content_type="application/json")


#USUARIO
@csrf_exempt
def usuario_criar(request):
    try:
        if request.method != 'POST':
            raise Exception(u'Nao eh um POST valido!')

        usu             = Usuario()
        usu.nome        = request.POST.get('nome')
        usu.email       = request.POST.get('email')
        usu.telefone    = request.POST.get('telefone')
        usu.set_password(request.POST.get('senha'))
        usu.save()

        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")

@csrf_exempt
def usuario_verificar_login(request):
    try:
        if request.method != 'POST':
            raise Exception(u'Nao eh um POST valido!')

        usu = Usuario.objects.get(email=request.POST.get('email'))

        if check_password(request.POST.get('senha'), usu.password):
            #localizar se usuario possui taxi
            taxi_id = 0
            ls_taxi = Taxi.objects.filter(usuario=usu)
            if len(ls_taxi):
                taxi_id = ls_taxi[0].id

            return HttpResponse('{"resultado":"OK", "id":"'+str(usu.id)+'","taxi_id":"'+str(taxi_id)+'"}', content_type="application/json")
        else:
            return HttpResponse('{"resultado":"ERRO", "mensagem":"Login ou senha invalidos"}', content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":"'+e.message+'"}', content_type="application/json")

def usuario_buscar(request, usuario_id):
    try:
        return HttpResponse(serializers.serialize("json", Usuario.objects.filter(pk=usuario_id)), content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")


#SOLICITACAO
@csrf_exempt
def solicitacao_criar(request):
    try:
        if request.method != 'POST':
            raise Exception(u'Nao eh um POST valido!')

        sol             = Solicitacao()
        sol.usuario     = Usuario.objects.get(pk=request.POST.get('usuario_id'))
        sol.taxi        = Taxi.objects.get(pk=request.POST.get('taxi_id'))
        sol.latitude    = request.POST.get('latitude')
        sol.longitude   = request.POST.get('longitude')
        sol.save()

        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")

def solicitacao_excluir(request, solicitacao_id):
    try:
        sol = Solicitacao.objects.get(pk=solicitacao_id)
        sol.delete()
        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")

def solicitacoes_usuario(request, usuario_id):
    try:
        return HttpResponse(serializers.serialize("json", Solicitacao.objects.filter(usuario=usuario_id)), content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")

def solicitacoes_taxi(request, taxi_id):
    try:
        return HttpResponse(serializers.serialize("json", Solicitacao.objects.filter(taxi=taxi_id)), content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")

def solicitacao_atender(request, solicitacao_id):
    try:
        solicitacao = Solicitacao.objects.get(pk=solicitacao_id)
        solicitacao.data_conclusao = datetime.datetime.now()
        return HttpResponse('{"resultado":"OK"}', content_type="application/json")
    except Exception as e:
        return HttpResponse('{"resultado":"ERRO", "mensagem":'+e.message+'}', content_type="application/json")