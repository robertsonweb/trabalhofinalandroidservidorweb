# coding: utf-8
from django.contrib import admin
from django.utils.translation import ungettext, ugettext as _
from models import *

#Usuario
class UsuarioAdmin(admin.ModelAdmin):
    list_display    =   ('nome','email')
    search_fields   =   list_display

admin.site.register(Usuario, UsuarioAdmin)


#Taxi
class TaxiAdmin(admin.ModelAdmin):
    list_display    =   ('veiculo','usuario', 'situacao')
    search_fields   =   list_display

admin.site.register(Taxi, TaxiAdmin)

#Solicitacao
class SolicitacaoAdmin(admin.ModelAdmin):
    list_display    =   ('data_solicitacao', 'usuario', 'taxi', 'data_conclusao')
    search_fields   =   list_display

admin.site.register(Solicitacao, SolicitacaoAdmin)