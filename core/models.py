# coding: utf-8
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _

#USUARIO
class UserManager(BaseUserManager):
    def create_user(self, nome, telefone, email, password):
        user = self.model(
            nome        = nome,
            email       = email,
            password    = password,
            telefone    = telefone
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(self, email, password)
        user.is_active = True
        user.save(using=self._db)
        return user

class Usuario(AbstractBaseUser):
    USERNAME_FIELD = 'email'

    objects = UserManager()
    
    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    def get_short_name(self):
        return self.nome[0:10]

    def get_full_name(self):
        return self.nome

    def is_staff(self):
        return self.is_staff

    nome        =   models.CharField(_('nome'), max_length=70)
    email       =   models.EmailField(_('email'), unique=True, max_length=70, db_index=True)
    telefone    =   models.CharField(_('telefone'), max_length=14)

    class Meta:
        ordering            =   [u'nome']
        verbose_name        =   _(u'usuário')
        verbose_name_plural =   _(u'usuários')

    def __unicode__(self):
        return self.nome

    @property
    def get_dados(self):
        qs_taxi = Taxi.objects.filter(usuario=self)
        if qs_taxi:
            return u"Veículo: %s - Taxista: %s" % (qs_taxi[0].veiculo, self.get_short_name())
        return ""

    @property
    def get_situacao(self):
        return Taxi.objects.get(usuario=self).get_situacao

    @property
    def get_taxi_id(self):
        return Taxi.objects.get(usuario=self).pk

    @property
    def get_latitude(self):
        taxi = Taxi.objects.get(usuario=self)
        return taxi.latitude

    @property
    def get_longitude(self):
        taxi = Taxi.objects.get(usuario=self)
        return taxi.longitude

#TAXI
class Taxi(models.Model):

    SITUACOES = (
        ('L',_('Livre')),
        ('O',_('Ocupado')),
    )

    veiculo     = models.CharField(_('veiculo'), max_length=70)
    usuario     = models.ForeignKey('Usuario', unique=True)
    latitude    = models.CharField(_('latitude'), max_length=20)
    longitude   = models.CharField(_('longitude'), max_length=20)
    situacao    = models.CharField(_(u'situação'), max_length=1, choices=SITUACOES)

    class Meta:
        ordering               = [u'usuario', u'veiculo']
        verbose_name           =   _(u'taxi')
        verbose_name_plural    =   _(u'taxi')

    def __unicode__(self):
        return self.usuario.nome+' - '+self.veiculo

    @property
    def get_situacao(self):
        if self.situacao=='O':
            return u"Ocupado"
        else:
            return u"Livre"

#SOLICITACAO
class Solicitacao(models.Model):
    data_solicitacao    = models.DateTimeField(_('data da solicitação'), auto_now_add=True)
    data_conclusao      = models.DateTimeField(_('data da conclusão'), null=True, blank=True)
    usuario             = models.ForeignKey('Usuario')
    taxi                = models.ForeignKey('Taxi')
    latitude            = models.CharField(_('latitude'), max_length=20)
    longitude           = models.CharField(_('longitude'), max_length=20)

    class Meta:
        ordering            =   [u'data_solicitacao']
        verbose_name        =   _(u'solicitação')
        verbose_name_plural =   _(u'solicitações')

    def __unicode__(self):
        return self.data_solicitacao.strftime("%d/%m/%Y %H:%M:%S")+' - '+self.usuario.nome+' - '+self.taxi.veiculo