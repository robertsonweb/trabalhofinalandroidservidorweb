from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('core.views',
    url(r'^$', 'home', name='home'),
    url(r'^meu-local$', 'meu_local', name='meu_local'),
    url(r'^minhas-solicitacoes-pendentes$', 'taxi_solicitacoes_pendentes', name='taxi_solicitacoes_pendentes'),
    url(r'^minhas-solicitacoes-atendidas$', 'taxi_solicitacoes_atendidas', name='taxi_solicitacoes_atendidas'),
    url(r'^alterar-minha-situacao$', 'taxi_alterar_minha_situacao', name='taxi_alterar_minha_situacao'),
    url(r'^solicitacao/(?P<solicitacao_id>\d+)/$', 'taxi_solicitacao', name='taxi_solicitacao'),
    url(r'^solicitacao-alterar-situacao/(?P<solicitacao_id>\d+)/$', 'taxi_solicitacao_alterar_situacao', name='taxi_solicitacao_alterar_situacao'),
    url(r'^taxi/buscar/(?P<taxi_id>\d+)/$', 'taxi_buscar', name='taxi_buscar'),
    url(r'^taxi/livres/$', 'taxi_livres', name='taxi_livres'),
    url(r'^taxi/ocupados/$', 'taxi_ocupados', name='taxi_ocupados'),
    url(r'^taxi/alterar-situacao/(?P<taxi_id>\d+)/$', 'taxi_alterar_situacao', name='taxi_alterar_situacao'),
    url(r'^taxi/alterar-posicao/(?P<taxi_id>\d+)/(?P<latitude>[\w|\W]+)/(?P<longitude>[\w|\W]+)$', 'taxi_alterar_posicao', name='taxi_alterar_posicao'),
    url(r'^taxi/solicitacoes/(?P<taxi_id>\d+)/$', 'solicitacoes_taxi', name='solicitacoes_taxi'),
    url(r'^taxi/solicitacao/atender/(?P<solicitacao_id>\d+)/$', 'solicitacao_atender', name='solicitacao_atender'),
    url(r'^usuario/criar', 'usuario_criar', name='usuario_criar'),
    url(r'^usuario/buscar/(?P<usuario_id>\d+)/$', 'usuario_buscar', name='usuario_buscar'),
    url(r'^usuario/solicitacao/criar', 'solicitacao_criar', name='solicitacao_criar'),
    url(r'^usuario/solicitacoes/(?P<usuario_id>\d+)/$', 'solicitacoes_usuario', name='solicitacoes_usuario'),
    url(r'^usuario/login', 'usuario_verificar_login', name='usuario_verificar_login'),
    url(r'^solicitacao/excluir/(>P<solicitacao_id>\d+)/$', 'solicitacao_excluir', name='solicitacao_excluir'),
    # Examples:
    # url(r'^$', 'taxiserver.views.home', name='home'),
    # url(r'^taxiserver/', include('taxiserver.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    url(r'^accounts/login/$', 'django.contrib.auth.views.login',{'template_name':'accounts/login.html'}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^password_change/$', 'django.contrib.auth.views.password_change', {'template_name':'accounts/password_change.html'}, name='password_change'),
    url(r'^password_change/done/$', 'django.contrib.auth.views.password_change_done', name='password_change_done'),
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', {'template_name':'accounts/password_reset.html'}, name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),
)

from django.conf import settings
urlpatterns += patterns('django.views.static',
    url(r'^static/(?P<path>.*)$', 'serve',
        {'document_root': settings.STATIC_ROOT}),
)