Servidor
----------------

URLs (Parâmetros e retornos)
----------------

BUSCAR INFORMAÇÕES DE UM TAXI
http://trab.ersistemas.info/taxi/buscar/<taxi_id>
Parâmetros:
<taxi_id> id to taxi que se está buscando
Retorna JSON:
pk, latitude, longitude, veiculo, situacao, usuario(motorista)
----------------

LISTAR TAXIS LIVRES
http://trab.ersistemas.info/taxi/livres
Retorna Lista JSON
pk, latitude, longitude, veiculo, situacao, usuario(motorista)
----------------

LISTAR TAXIS OCUPADOS
http://trab.ersistemas.info/taxi/ocupados
Retorna Lista JSON
pk, latitude, longitude, veiculo, situacao, usuario(motorista)
----------------

ALTERNAR SITUAÇÃO DO TAXI (LIVRE/OCUPADO)
http://trab.ersistemas.info/taxi/alterar-situacao/<taxi_id>
Parâmetros:
<taxi_id> id do taxi que será alterado a situação
se o taxi estava livre passa a ser ocupado e vice-versa
JSON
resultado = Ok para operação realizada com sucesso
resultado = ERRO para erro ao executar a operação
----------------

ALTERNAR POSIÇÃO GEOGRÁFICA DO TAXI
http://trab.ersistemas.info/taxi/alterar-posicao/<taxi_id>/<latitude>/<longitude>
Parâmetros:
<taxi_id> id do taxi que será alterado a posição
<latitude> ponto de latitude do taxi
<longitude> ponto de longitude do taxi
JSON
resultado = Ok para operação realizada com sucesso
resultado = ERRO para erro ao executar a operação
----------------


LISTAR AS SOLICITAÇÕES DO TAXI EM QUESTÃO
http://trab.ersistemas.info/taxi/solicitacoes/<taxi_id>
Parâmetros:
<taxi_id> id do taxi que está querendo ver as solicitações que recebeu
lista JSON com as solicitações
pk da solcitação, data_solicitacao, taxi_id, usuario_id, data_conclusao, latitude, longitude
----------------

ATENDER UMA SOLICITAÇÃO
Consiste em colocar a data de conclusao de uma solicitação
http://trab.ersistemas.info/taxi/solicitacao/atender/<solicitacao_id>
Parâmetros:
<solicitacao_id> id da solicitacao que deve ser concluída
JSON
resultado = Ok para operação realizada com sucesso
resultado = ERRO para erro ao executar a operação
mensagem = caso existir erro, é passado neste campo os detalhes do erro
----------------

CRIAR UM NOVO USUÁRIO
Aceita requisições do método POST
http://trab.ersistemas.info/usuario/criar
Campos recebidos via POST:
<nome> obrigatório, nome do usuário
<email> obrigatório, e-mail do usuário (será utilizado como login)
<telefone> obrigatório, telefone do usuário
<senha> obrigatório, senha do usuário de modo limpo, pois a mesma é criptografada no servidor
JSON
resultado = Ok para operação realizada com sucesso
resultado = ERRO para erro ao executar a operação
mensagem = caso existir erro, é passado neste campo os detalhes do erro
----------------

BUSCAR INFORMAÇÕES DE UM USUÁRIO
http://trab.ersistemas.info/usuario/buscar/<usuario_id>
Parâmetros:
<usuario_id> id to usuario que se está buscando
Retorna JSON:
pk, nome, email, telefone, last_login
----------------

CRIAR UMA NOVA SOLICITAÇÃO
Aceita requisições do método POST
http://trab.ersistemas.info/usuario/solicitacao/criar
Campos recebidos via POST:
<usuario_id> obrigatório, id do usuário solicitante
<taxi_id> obrigatório, id do taxi que está sendo solicitado
<latitude> obrigatório, latitude do ponto onde está sendo solicitado
<longitue> obrigatório, longitude do ponto onde está sendo solicitado

JSON
resultado = Ok para operação realizada com sucesso
resultado = ERRO para erro ao executar a operação
mensagem = caso existir erro, é passado neste campo os detalhes do erro
----------------

LISTAR AS SOLICITAÇÕES DO USUÁRIO EM QUESTÃO
http://trab.ersistemas.info/usuario/solicitacoes/<usuario_id>
Parâmetros:
<taxi_id> id do usuário que está querendo ver suas solicitações
lista JSON com as solicitações
pk da solcitação, data_solicitacao, taxi_id, usuario_id, data_conclusao, latitude, longitude
----------------

LOGAR USUÁRIO (TANTO FAZ... USUÁRIO CLIENTE OU USUÁRIO MOTORISTA)
Aceita requisições do método POST
http://trab.ersistemas.info/usuario/login
Campos recebidos via POST:
<email> obrigatório, e-mail do usuário (será utilizado como login)
<senha> obrigatório, senha do usuário de modo limpo, pois a mesma é criptografada no servidor
JSON
resultado = Ok logado com sucesso
id= id do usuário logado
taxi_id = se diferente de zero, indica o id do taxi deste usuário caso ele for um taxista (motorista)
resultado = ERRO para erro efetuar o login
mensagem = caso existir erro, é passado neste campo os detalhes do erro
----------------

EXCLUIR UMA SOLICITAÇÃO
http://trab.ersistemas.info/solicitacao/excluir/<solicitacao_id>
Parâmetros:
<solicitacao_id> id da solicitação que será excluída
JSON
resultado = Ok operação executada com sucesso
resultado = ERRO para erro ao executar a operação
mensagem = caso existir erro, é passado neste campo os detalhes do erro
----------------
